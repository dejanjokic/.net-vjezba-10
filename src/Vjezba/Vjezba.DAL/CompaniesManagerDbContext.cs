﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Vjezba.Model;
using System.Data.Entity;


namespace Vjezba.DAL
{
    public class CompaniesManagerDbContext : IdentityDbContext<User>
    {
        public DbSet<Company> Companies { get; set; }

        public DbSet<City> Cities { get; set; }

        public CompaniesManagerDbContext()
            : base("CompaniesManagerDbContext")
        {

        }

        public static CompaniesManagerDbContext Create()
        {
            return new CompaniesManagerDbContext();
        }

    }
}