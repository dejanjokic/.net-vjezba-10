namespace Vjezba.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntityBase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cities", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Cities", "DateModified", c => c.DateTime());
            AddColumn("dbo.Companies", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Companies", "DateModified", c => c.DateTime());
            AddColumn("dbo.CompanyContacts", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.CompanyContacts", "DateModified", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CompanyContacts", "DateModified");
            DropColumn("dbo.CompanyContacts", "DateCreated");
            DropColumn("dbo.Companies", "DateModified");
            DropColumn("dbo.Companies", "DateCreated");
            DropColumn("dbo.Cities", "DateModified");
            DropColumn("dbo.Cities", "DateCreated");
        }
    }
}
