namespace Vjezba.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "OIB", c => c.String());
            AddColumn("dbo.AspNetUsers", "JMBG", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "JMBG");
            DropColumn("dbo.AspNetUsers", "OIB");
        }
    }
}
