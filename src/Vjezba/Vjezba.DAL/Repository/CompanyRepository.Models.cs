﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjezba.DAL.Repository
{
    public interface ICompanyFilter
    {
        string Name { get; }

        string Email { get; }

        string Address { get; }

        string CityName { get; }
    }
}