﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vjezba.Model;

namespace Vjezba.DAL.Repository
{
    public class CompanyRepository : RepositoryBase<Company>
    {
        public CompanyRepository(CompaniesManagerDbContext context)
            : base(context)
        {
        }

        public List<Company> GetList(ICompanyFilter filter)
        {
            var companiesQuery = this.DbContext.Companies
                .Include(p => p.City)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(filter?.Name))
                companiesQuery = companiesQuery.Where(p => p.Name.Contains(filter.Name));

            if (!string.IsNullOrWhiteSpace(filter?.Address))
                companiesQuery = companiesQuery.Where(p => p.Address.Contains(filter.Address));

            if (!string.IsNullOrWhiteSpace(filter?.Email))
                companiesQuery = companiesQuery.Where(p => p.Email.Contains(filter.Email));

            if (!string.IsNullOrWhiteSpace(filter?.CityName))
                companiesQuery = companiesQuery.Where(p => p.City.Name.Contains(filter.CityName));

            return companiesQuery
                .OrderBy(p => p.ID)
                .ToList();
        }

        public override Company Find(int id)
        {
            return this.DbContext.Companies
                .Include(p => p.City)
                .Where(p => p.ID == id)
                .FirstOrDefault();
        }
    }
}
