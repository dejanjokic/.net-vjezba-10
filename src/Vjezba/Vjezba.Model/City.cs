﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vjezba.Model
{
    public class City : EntityBase
    {
        public string PostalCode { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Company> Companies { get; set; }
    }

}