﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Vjezba.Model
{
    public class Company : EntityBase
    {
        [Required]
        [StringLength(100, MinimumLength = 5)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email je obvezno unijeti (nesto@nesto.com)")]
        public string Email { get; set; }

        public string Address { get; set; }

        [Range(-90, 90)]
        public decimal Latitude { get; set; }

        [Range(-180, 180)]
        public decimal Longitude { get; set; }

        public DateTime DateFrom { get; set; }

        [ForeignKey("City")]
        public int CityID { get; set; }

        public City City { get; set; }

        public virtual ICollection<CompanyContact> Contacts { get; set; }

    }

}