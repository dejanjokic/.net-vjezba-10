﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vjezba.Web.Models;
using Vjezba.Model;
using Vjezba.DAL;

namespace Vjezba.Web.Areas.Admin.Controllers
{
    [RouteArea("Admin", AreaPrefix = "Administracija")]
    [RoutePrefix("Gradovi")]
    public class CityController : Controller
    {
        [Route("")]
        public ActionResult Index()
        {
            var context = new CompaniesManagerDbContext();
            var cities = context.Cities.ToList();
            context.Dispose();

            //Naziv view-a se automatski skuzi iz naziva akcije controller-a, dovoljno je proslijediti samo model
            return View(cities);
        }

        [Route("detalji")]
        public ActionResult Details(int? id = null)
        {
            if (id == null)
                return View();

            var context = new CompaniesManagerDbContext();
            var model = context.Cities.Find(id.Value);
            context.Dispose();

            return View(model);
        }

        [Route("detalji/info")]
        public ActionResult DetailsPartial(int? id = null)
        {
            if (id == null)
                return null;

            var context = new CompaniesManagerDbContext();
            var model = context.Cities.Find(id.Value);
            context.Dispose();

            if (model == null)
                return null;

            return PartialView("_DetailsPartial", model);
        }
    }

}