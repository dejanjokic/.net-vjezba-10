﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ninject;
using Vjezba.DAL.Repository;
using Vjezba.Model;
using Vjezba.Web.Models;

namespace Vjezba.Web.Controllers
{
    [RequireHttps]
    public class CompanyController : Controller
    {
        [Inject]
        public CompanyRepository CompanyRepository { get; set; }

        [Inject]
        public CityRepository CityRepository { get; set; }

        public ActionResult Index()
        {
            return this.View(this.CompanyRepository.GetList(null));
        }

        [HttpPost]
        public ActionResult IndexAjax(CompanyFilterModel model)
        {
            return PartialView("_IndexTable", this.CompanyRepository.GetList(model));
        }

        public ActionResult Create()
        {
            this.FillDropDownValues();
            return View();
        }

        [HttpPost]
        public ActionResult Create(Company model)
        {
            if (ModelState.IsValid)
            {
                this.CompanyRepository.Add(model, autoSave: true);

                return RedirectToAction("Index");
            }
            else
            {
                this.FillDropDownValues();
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            this.FillDropDownValues();

            var model = this.CompanyRepository.Find(id);

            return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditPost(int id)
        {
            var model = this.CompanyRepository.Find(id);
            var didUpdateModelSucceed = this.TryUpdateModel(model);

            if (didUpdateModelSucceed && ModelState.IsValid)
            {
                this.CompanyRepository.Update(model, autoSave: true);
                return RedirectToAction("Index");
            }

            this.FillDropDownValues();
            return View(model);
        }

        public ActionResult Details(int? id = null)
        {
            if (id == null)
                return View();

            var model = this.CompanyRepository.Find(id.Value);

            return View(model);
        }

        public JsonResult Delete(int id)
        {
            this.CompanyRepository.Delete(id);
            this.CompanyRepository.Save();

            return new JsonResult() { Data = "OK", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        private void FillDropDownValues()
        {
            var possibleCities = this.CityRepository.GetList();

            var selectItems = new List<System.Web.Mvc.SelectListItem>();

            //Polje je opcionalno
            var listItem = new SelectListItem();
            listItem.Text = "- odaberite -";
            listItem.Value = "";
            selectItems.Add(listItem);

            foreach (var city in possibleCities)
            {
                listItem = new SelectListItem();
                listItem.Text = city.Name;
                listItem.Value = city.ID.ToString();
                listItem.Selected = false;
                selectItems.Add(listItem);
            }

            ViewBag.PossibleCities = selectItems;
        }
    }
}